import django.utils.timezone as tz
from django.conf import settings
from django.db import models

__all__ = ['ParkingLot', 'ParkingLotDate', 'TimeSlotUser', 'ParkingTimeSlot']


class ParkingLot(models.Model):
    lot_owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="parking_lots",
    )

    name = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return (
            f'{type(self).__name__}(id={self.id}, {self.name}, '
            f'{self.lot_owner.email})'
        )

    def __repr__(self):
        return self.__str__()


class ParkingLotDate(models.Model):
    parking_lot = models.ForeignKey(
        ParkingLot, on_delete=models.CASCADE, related_name="dates"
    )

    date = models.DateField(default=tz.now, blank=False, null=False)


class TimeSlotUser(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    email = models.EmailField(blank=False, null=False)
    phone_number = models.CharField(max_length=12, blank=False, null=False)

    def __str__(self):
        return f'{type(self).__name__}(id={self.id}, {self.email})'

    def __repr__(self):
        return self.__str__()


class ParkingTimeSlot(models.Model):
    slot_date = models.ForeignKey(
        ParkingLotDate, on_delete=models.CASCADE, related_name="time_slots"
    )
    slot_user = models.ForeignKey(
        TimeSlotUser,
        on_delete=models.SET_NULL,
        related_name="parking_slots",
        null=True,
    )

    start_time = models.TimeField(
        default=tz.datetime.strptime('00:00', '%H:%M').time(),
        blank=False,
        null=False,
    )
    end_time = models.TimeField(
        default=tz.datetime.strptime('23:59', '%H:%M').time(),
        blank=False,
        null=False,
    )
    booked = models.BooleanField(default=False)

    def __str__(self):
        if self.slot_user:
            email = f'{self.slot_user.email}, '
        else:
            email = ''

        return (
            f'{type(self).__name__}(id={self.id}, {email}'
            f'{self.slot_date.date.strftime("%d %b %Y")}, '
            f'{self.start_time.strftime("%H:%M")}, '
            f'{self.end_time.strftime("%H:%M")})'
        )

    def __repr__(self):
        return self.__str__()
