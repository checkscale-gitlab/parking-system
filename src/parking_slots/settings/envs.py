import os

from .defaults import Defaults


class BaseDevelopment(Defaults):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    DEBUG = True

    SECRET_KEY = 'CHANGEME!!'
    SECURE_SSL_REDIRECT = False
    SECURE_BROWSER_XSS_FILTER = False
    SECURE_CONTENT_TYPE_NOSNIFF = False

    INTERNAL_IPS = ['127.0.0.1']
    DEBUG_TOOLBAR_ENABLED = False
    DEBUG_TOOLBAR_PANELS = [
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        # 'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        # 'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        # 'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        # 'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
        # 'debug_toolbar.panels.profiling.ProfilingPanel',
    ]

    @property
    def TEMPLATES(self):
        templates = super().TEMPLATES
        templates[0]['OPTIONS']['debug'] = True
        return templates

    @property
    def INSTALLED_APPS(self):
        apps = ['whitenoise.runserver_nostatic']
        if self.DEBUG_TOOLBAR_ENABLED:
            apps.insert(0, 'debug_toolbar')
        return apps + super().INSTALLED_APPS

    @property
    def MIDDLEWARE(self):
        middleware = super().MIDDLEWARE
        if self.DEBUG_TOOLBAR_ENABLED:
            middleware.insert(
                0, 'debug_toolbar.middleware.DebugToolbarMiddleware'
            )
        return middleware


class Testing(Defaults):
    ALLOWED_HOSTS = []


class Production(Defaults):
    ALLOWED_HOSTS = []
