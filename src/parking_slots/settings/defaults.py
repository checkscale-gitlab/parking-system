import os

from configurations import Configuration, values


class Defaults(Configuration):
    """Base settings object, settings in this class will be shared
    over all different environments.

    """

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    BASE_CONTAINER_DIR = '/opt/parking_slots'

    DEBUG = False

    DATABASES = values.DatabaseURLValue(
        'pgsql://postgres:postgres@db:5432/toogethr'
    )

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        # custom apps
        'parking_slots',
        'parking_slots.apps.users',
    ]

    MIDDLEWARE = [
        'django.middleware.http.ConditionalGetMiddleware',
        'django.middleware.security.SecurityMiddleware',
        'whitenoise.middleware.WhiteNoiseMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'django.middleware.http.ConditionalGetMiddleware',
    ]

    ROOT_URLCONF = 'parking_slots.urls'

    SECRET_KEY = values.SecretValue()

    @property
    def TEMPLATES(self):
        return [
            {
                'BACKEND': 'django.template.backends.django.DjangoTemplates',
                'DIRS': [os.path.join(self.BASE_DIR, 'templates')],
                'APP_DIRS': True,
                'OPTIONS': {
                    'context_processors': [
                        'django.template.context_processors.debug',
                        'django.template.context_processors.request',
                        'django.contrib.auth.context_processors.auth',
                        'django.contrib.messages.context_processors.messages',
                    ]
                },
            }
        ]

    WSGI_APPLICATION = 'parking_slots.wsgi.application'

    AUTH_USER_MODEL = 'users.User'
    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'  # NOQA:E501
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'  # NOQA:E501
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'  # NOQA:E501
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'  # NOQA:E501
        },
    ]

    LANGUAGE_CODE = 'en'
    TIME_ZONE = 'Europe/Amsterdam'

    USE_I18N = False
    USE_L10N = True
    USE_TZ = True

    SECURE_SSL_REDIRECT = True
    SECURE_BROWSER_XSS_FILTER = True
    SECURE_CONTENT_TYPE_NOSNIFF = True

    @property
    def STATIC_ROOT(self):
        return os.path.join(self.BASE_DIR, 'public', 'static')

    @property
    def MEDIA_ROOT(self):
        return os.path.join(self.BASE_DIR, 'public', 'media')

    STATIC_URL = '/static/'
    MEDIA_URL = '/media/'
