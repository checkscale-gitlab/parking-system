from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

from .views import (
    ParkingOwnersView,
    ThanksView,
    TimeSlotsDashboardView,
    TimeSlotsView,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'robots.txt',
        TemplateView.as_view(
            template_name='robots.txt', content_type='text/plain'
        ),
    ),
    path('', ParkingOwnersView.as_view(), name='index'),
    path('owner/<int:owner_id>/', TimeSlotsView.as_view(), name='owner'),
    path('dashboard/', TimeSlotsDashboardView.as_view(), name='dashboard'),
    path('thanks/', ThanksView.as_view(), name='thanks'),
]

if settings.DEBUG:
    from django.views import defaults  # noqa isort:skip

    # Try to load the django-debug-toolbar if it's available.
    try:
        import debug_toolbar  # noqa isort:skip
    except ImportError:
        pass
    else:
        urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))
