from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils import timezone as tz
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from .forms import TimeSlotForm
from .lib.time_slots import get_free_parking_slots, get_parking_slots_dashboard
from .models import ParkingTimeSlot


class ParkingOwnersView(TemplateView):
    template_name = 'parking_slots/parking_owners.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        User = get_user_model()
        ctx['parking_owners'] = User.objects.exclude(parking_lots__isnull=True)

        return ctx


class TimeSlotsDashboardView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/login/'
    template_name = 'parking_slots/time_slots_dashboard.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['time_slots'] = get_parking_slots_dashboard(self.request.user.pk)

        return ctx


class TimeSlotsView(FormView):
    template_name = 'parking_slots/time_slots.html'
    form_class = TimeSlotForm
    success_url = reverse_lazy('thanks')

    def get_success_url_with_params(self, booked_slot_id: int):
        return f"{self.success_url}?slot={booked_slot_id}"

    def get(self, request, *args, **kwargs):
        owner_id = self.kwargs.get('owner_id', 0)
        User = get_user_model()
        lot_owner = User.objects.filter(pk=owner_id).exists()
        if not lot_owner:
            messages.add_message(
                self.request,
                messages.ERROR,
                'You have chosen incorrect parking lot owner',
            )
            return redirect('index')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        owner_id = self.kwargs.get('owner_id', 0)
        User = get_user_model()
        lot_owner = User.objects.filter(pk=owner_id).first()

        ctx['owner'] = lot_owner
        ctx['time_slots'] = get_free_parking_slots(owner_id)
        ctx['date'] = tz.now().date().strftime("%d %b %Y")

        return ctx

    def get_initial(self):
        self.initial['owner_id'] = self.kwargs.get('owner_id', 0)
        return super().get_initial()

    def form_valid(self, form):
        time_slot_id = form.cleaned_data.get('booked_time_slot_id', 0)

        return HttpResponseRedirect(
            self.get_success_url_with_params(time_slot_id)
        )


class ThanksView(TemplateView):
    template_name = 'parking_slots/thanks.html'

    def get(self, request, *args, **kwargs):
        try:
            slot_id = int(request.GET.get('slot', 0))
        except ValueError:
            return redirect('index')

        time_slot = (
            ParkingTimeSlot.objects.select_related(
                'slot_user', 'slot_date__parking_lot__lot_owner'
            )
            .filter(pk=slot_id)
            .first()
        )
        if not time_slot:
            return redirect('index')

        kwargs['time_slot'] = time_slot

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        time_slot = kwargs.get('time_slot')
        ctx['time_slot'] = time_slot

        return ctx
