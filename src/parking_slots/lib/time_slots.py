from django.utils import timezone as tz

from parking_slots.models import ParkingLot, ParkingLotDate, ParkingTimeSlot


def get_parking_slots_dashboard(owner_id):
    parking_lots = ParkingLot.objects.filter(lot_owner_id=owner_id).all()
    number_of_dates = ParkingLotDate.objects.filter(
        date=tz.now().date(), parking_lot__lot_owner_id=owner_id
    ).count()

    if number_of_dates != parking_lots.count():
        for _parking_lot in parking_lots:
            _slot_date = ParkingLotDate.objects.create(
                parking_lot_id=_parking_lot.pk, date=tz.now().date()
            )
            ParkingTimeSlot.objects.create(slot_date_id=_slot_date.pk)

    time_slots = (
        ParkingTimeSlot.objects.select_related(
            'slot_user', 'slot_date__parking_lot'
        )
        .filter(
            slot_date__date=tz.now().date(),
            slot_date__parking_lot__lot_owner_id=owner_id,
        )
        .all()
        .order_by('slot_date__parking_lot__name', 'start_time', 'end_time')
    )

    return time_slots


def get_free_parking_slots(owner_id):
    parking_lots = ParkingLot.objects.filter(lot_owner_id=owner_id).all()
    number_of_dates = ParkingLotDate.objects.filter(
        date=tz.now().date(), parking_lot__lot_owner_id=owner_id
    ).count()

    if number_of_dates != parking_lots.count():
        for _parking_lot in parking_lots:
            _slot_date = ParkingLotDate.objects.create(
                parking_lot_id=_parking_lot.pk, date=tz.now().date()
            )
            ParkingTimeSlot.objects.create(slot_date_id=_slot_date.pk)

    free_slots = (
        ParkingTimeSlot.objects.distinct('start_time', 'end_time')
        .filter(
            slot_date__date=tz.now().date(),
            slot_date__parking_lot__lot_owner_id=owner_id,
            booked=False,
        )
        .order_by('start_time', 'end_time')
    )

    return free_slots
