from setuptools import find_packages, setup


debug_require = [
    'black==19.3b0',  # format/refactoring code
    'django-debug-toolbar',
    'flake8>=3.7,<3.8',  # check code on PEP-8 rules
    'flake8-blind-except>=0.1,<0.2',  # search wide try except usage
    'flake8-bugbear>=19.3,<19.4',  # search for possible bugs
    'flake8-comprehensions>=2.1,<2.2',  # NOQA: find where is better to use comprehensions
    'flake8-isort>=2.7,<2.8',  # check if imports are sorted
    'flake8-mutable>=1.2,<1.3',  # search for mutable args
    'isort>=4.3,<4.4',  # configurable imports sort
    'isort>=4.3,<4.4',  # configurable imports sort
]

tests_require = [
    'factory_boy>=2.12,<2.13',  # data factories
    'Faker>=2.0,<2.1',  # create fake data
    'pytest-cov>=2.7,<2.8',  # tests coverage
    'pytest-django>=3.5.1,<3.6',  # tests, pytest
    'pytest-mock>=1.10,<1.11',  # patch(mock) modules
]

setup(
    author='Andrew Voronov',
    author_email='voronov.a.g@gmail.com',
    name='parking_slots',
    description='',
    version='0.1.0',
    url='https://example.com/',
    install_requires=[
        'dj-database-url==0.5.0',  # config DB from url
        'django>=2.2,<2.3',
        'django-configurations==2.1',  # settings in class
        'requests>=2.22,<2.23',
        'psycopg2-binary>=2.7,<2.8',  # PSQL connection
        'whitenoise==4.1',  # storing static files on server
        'wsgi-basic-auth>=1.1,<1.2',
        *tests_require
    ],
    extras_require={
        'test': tests_require,
        'dev': debug_require + tests_require,
    },
    tests_require=tests_require,
    scripts=['manage.py'],
    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,
    license='',
    classifiers=[
        'Private :: Do Not Upload',
        'License :: Other/proprietary License',
    ],
    zip_safe=False,
)
