# parking_slots


## Requirements

 * Python 3.7
 * PostgreSQL > 9.6
 
 
 ## Project description

On the `index` page user can see all parking lots owners(in test data only 1 owner, Bob, was created).

Parking lot owner can have multiple parking lots(in test data only 1 parking lot was created)

User chooses a lot owner and based on choice redirects to a parking slot booking page. 
On this page user see only `FREE` time slots for `TODAY`. User needs to fill contact info and time slot period. Time slot 
period can be >= 1 minute. If the filled form is correct and the time slot is available for booking the user will get a 
success message. There is no limits for bookings per user per day.

Lot owner can view the `DASHBOARD`: all info about all parking lots. Lot owner must be logged in to view a dashboard link. 
Currenly login works using admin login url (`http://127.0.0.1/admin/`). In test data lot owner Bob is admin. 
Login credentials are:

email: `admin@admin.com`
password: `superadmin` 


### Docker

Create a virtualenv (assuming you have virtualenvwrapper installed)

    mkvirtualenv parking_slots

To run the project in your local docker environment do the following steps:

    1. Make a local.py in your settings folder(read the `README.md` in the settings folder) 
    Set database settings:
```python
import dj_database_url


class LocalSettings:
    DATABASES = {
        'default': dj_database_url.parse(
            'pgsql://postgres:postgres@db:5432/toogethr'
        )
    }
```
    2. Run the command `make init`
    3. go to 127.0.0.1:8000
    
View all make commands:

    make help
    

## Installation without Docker

Create a virtualenv (assuming you have virtualenvwrapper installed)

    mkvirtualenv parking_slots

Create the database using postgres:
   
    createdb parking_slots_db

To install the required python dependencies and load test data run:

    1. Make a local.py in your settings folder(read the `README.md` in the settings folder) 
        Set database settings:
```python
import dj_database_url


class LocalSettings:
    DATABASES = {
        'default': dj_database_url.parse(
            'pgsql://127.0.0.1/parking_slots_db'
        )
    }
```
    2. make requirements
    3. make migrate
    4. python manage.py loaddata fixtures/test.json

A superuser can be created via

    ./manage.py createsuperuser


### Running tests

To run the complete test suite in Docker, use

    make docker-test

To run the complete test suite, use

    make test


### Linting

Make sure your project is linted using `flake8` and `black`. This can be checked via

    make lint
    
### Code refactoring

Automatically reformat code based on best practices(`black`) use `fmt` command:

    make fmt
