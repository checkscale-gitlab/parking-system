import datetime as dt

import pytest

from parking_slots.forms import TimeSlotForm
from tests.factories.parking import ParkingTimeSlotFactory
from tests.factories.users import UserFactory


@pytest.mark.django_db
class TestTimeSlotForm:
    def setup(self):
        self.lot_owner = UserFactory()

    def test_incorrect_phone(self):
        form_data = {
            'name': 'John',
            'phone_number': 123,
            'email': 'test@test.com',
            'start_time': dt.time(9, 0),
            'end_time': dt.time(18, 0),
            'date': dt.datetime.now().date(),
            'owner_id': self.lot_owner.pk,
        }
        form = TimeSlotForm(data=form_data)

        assert not form.is_valid()
        assert len(form.errors) == 1
        assert 'phone_number' in form.errors

    def test_no_time_slot(self):
        form_data = {
            'name': 'John',
            'phone_number': '+31612344567',
            'email': 'test@test.com',
            'start_time': dt.time(9, 0),
            'end_time': dt.time(18, 0),
            'date': dt.datetime.now().date(),
            'owner_id': self.lot_owner.pk,
        }
        form = TimeSlotForm(data=form_data)

        assert not form.is_valid()
        assert len(form.errors) == 1
        assert '__all__' in form.errors

    def test_booked_time_slot(self):
        ParkingTimeSlotFactory(
            slot_date__parking_lot__lot_owner_id=self.lot_owner.pk,
            slot_user=None,
            start_time=dt.time(00, 0),
            end_time=dt.time(23, 59),
        )

        form_data = {
            'name': 'John',
            'phone_number': '+31612344567',
            'email': 'test@test.com',
            'start_time': dt.time(9, 0),
            'end_time': dt.time(18, 0),
            'date': dt.datetime.now().date(),
            'owner_id': self.lot_owner.pk,
        }
        form = TimeSlotForm(data=form_data)

        assert form.is_valid()
        assert form.cleaned_data['booked_time_slot_id']
