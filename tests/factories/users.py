import faker
from django.contrib.auth import get_user_model
from factory import DjangoModelFactory, LazyAttribute, Sequence

__all__ = ['UserFactory']


fake = faker.Factory.create()
User = get_user_model()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    password = Sequence(lambda u: fake.password())
    email = Sequence(lambda u: fake.email())
    username = LazyAttribute(lambda obj: obj.email)
