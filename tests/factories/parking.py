import datetime as dt
from random import randint

import faker
from factory import DjangoModelFactory, Sequence, SubFactory

from parking_slots import models

from .users import UserFactory

__all__ = [
    'ParkingLotFactory',
    'ParkingLotDateFactory',
    'TimeSlotUserFactory',
    'ParkingTimeSlotFactory',
]


fake = faker.Factory.create()


class ParkingLotFactory(DjangoModelFactory):
    class Meta:
        model = models.ParkingLot

    lot_owner = SubFactory(UserFactory)

    name = Sequence(lambda u: fake.name())


class ParkingLotDateFactory(DjangoModelFactory):
    class Meta:
        model = models.ParkingLotDate

    parking_lot = SubFactory(ParkingLotFactory)


class TimeSlotUserFactory(DjangoModelFactory):
    class Meta:
        model = models.TimeSlotUser

    email = Sequence(lambda u: fake.email())
    name = Sequence(lambda u: fake.name())
    phone_number = Sequence(
        lambda t: f"+316249560{randint(0,9)}{randint(0,9)}"
    )


class ParkingTimeSlotFactory(DjangoModelFactory):
    class Meta:
        model = models.ParkingTimeSlot

    slot_date = SubFactory(ParkingLotDateFactory)
    slot_user = SubFactory(TimeSlotUserFactory)

    start_time = dt.time(0, 0)
    end_time = dt.time(23, 59)
